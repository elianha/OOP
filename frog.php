<?php
require_once 'animal.php';

class Frog extends Animal{
    function jump(){
        echo "Jump : Hop hop";
    }
    
}
$kodok = new Frog("buduk",4,"no");
echo "Name : ".$kodok->getName(). "<br>";
echo "legs : ".$kodok->getLegs(). "<br>";
echo "cold blooded : ".$kodok->getCold(). "<br>";
echo $kodok->jump(). "<br><br>";
