<?php
require_once 'animal.php';

class Ape extends Animal{
    public $legs = 2;
    public function __construct($name,$legs,$cold_blooded)
    {
        $this->name = $name;
        $this->legs = 2;
        $this->cold_blooded = $cold_blooded;
    }
    function yell(){
        echo "Yell : Aaauooo\n";
    }
    function getLegs(){
        return $this->legs;
    }
}
$sungokong = new Ape("Kera",6,"no");
echo "Name : ".$sungokong->getName(). "<br>";
echo "legs : ".$sungokong->getLegs(). "<br>";
echo "cold blooded : ".$sungokong->getCold(). "<br>";
echo $sungokong->yell(). "<br><br>";

?>