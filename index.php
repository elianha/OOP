<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>OOP</title>
</head>
<body>
<?php
include 'animal.php';
include 'frog.php';
include 'ape.php';
$sheep = new Animal("Shaun",4,"No");
echo "Nama : ". $sheep->getName(). "<br>";
echo "legs : ". $sheep->getLegs(). "<br>";
echo "cold blooded : ".$sheep->getCold();

?>
    
</body>
</html>